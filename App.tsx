import React, { Component } from 'react';
import { Text } from 'react-native';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

import Loading from './src/Loading';
import RestaurantCarousel from './src/RestaurantCarousel';

const restaurantsQuery = gql`
    query getRests($address: String!) {
        search_restaurants(address: $address) {
            results {
                title
                address
                url
                phone
                image
                open_closed
                distance
                rating
            }
        }
    }
`;

interface Props {
    address?: string;
}

interface State {
    resolvedAddress: string | null;
}

export default class App extends Component<Props, State> {
    render() {
        const { address } = this.props;

        return address ?
                <Query query={restaurantsQuery} variables={{ address }}  >
                    {({ loading, error, data}) => {
                        if (loading) {
                            return <Loading />
                        }

                        if (error) {
                            return <Text>{error.message}</Text>
                        }

                        return <RestaurantCarousel address={address} data={data.search_restaurants.results.slice(0, 10)} />
                    }}
                </Query>
                : <Loading />
    }
}

