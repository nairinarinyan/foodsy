import React from 'react';
import { Navigation } from "react-native-navigation";
import { ApolloProvider } from 'react-apollo';
import { createHttpLink } from 'apollo-link-http';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory'
import { initScreens } from './src/screens';
import App from './App';
import colors from './src/common/colors';

const httpLink = createHttpLink({
    uri: 'https://food.act.today/graphql'
});

const client = new ApolloClient({
    link: httpLink,
    cache: new InMemoryCache()
});

const Root = props => (
    <ApolloProvider client={client}>
        <App {...props} />
    </ApolloProvider>
);

initScreens(Root);

Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setDefaultOptions({
        topBar: {
            background: {
                color: colors.blue
            },
            title: {
                alignment: 'center',
                color: colors.white
            }
        }
    });

    Navigation.setRoot({
        root: {
            component: {
                name: "foodsy.EntryScreen"
            }
        }
    });
});