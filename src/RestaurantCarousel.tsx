import React, { PureComponent } from 'react';
import {
    View,
    StyleSheet,
    Linking
} from 'react-native'
import Carousel from 'react-native-snap-carousel';

import SliderEntry, { IRestaurantData, itemWidth, sliderWidth } from './SliderEntry';
import colors from './common/colors';

interface Props {
    address: string;
    data: IRestaurantData[]
}

export default class RestaurantCarousel extends PureComponent<Props> {
    constructor(props: any) {
        super(props);

        this.state = {
            activeSlide: 0
        };
    }

    renderItemWithParallax = ({ item } : { item: IRestaurantData }, parallaxProps: any) => {
        return (
            <SliderEntry
                address={this.props.address}
                data={item}
                parallaxProps={parallaxProps}
                onPress={() => this.onItemPress(item.url)}
            />
        );
    }

    onItemPress(url: string) {
        Linking.openURL(url);
    }

    render() {
        const { data } = this.props;

        return (
            <View style={styles.container}>
                <Carousel
                  data={data}
                  renderItem={this.renderItemWithParallax}
                  sliderWidth={sliderWidth}
                  itemWidth={itemWidth}
                  hasParallaxImages={true}
                  firstItem={0}
                  inactiveSlideScale={0.90}
                  inactiveSlideOpacity={0.8}
                  containerCustomStyle={styles.slider}
                  contentContainerCustomStyle={styles.sliderContentContainer}
                  onSnapToItem={(index) => this.setState({ activeSlide: index }) }
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        paddingVertical: 30,
        backgroundColor: colors.lightBlue,
    },
    slider: {
        marginTop: 15,
        overflow: 'visible'
    },
    sliderContentContainer: {
        paddingVertical: 10
    },
});