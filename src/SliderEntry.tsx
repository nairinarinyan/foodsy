import React, { PureComponent } from 'react';
import {
    View,
    TouchableOpacity,
    Dimensions,
    StyleSheet,
    Linking,
    AsyncStorage
} from 'react-native';
import { ParallaxImage } from 'react-native-snap-carousel';

import RestaurantDetail, { RestaurantDetailType } from './RestaurantDetail';
import SlideEntryHeader from './SlideEntryHeader';
import SlideEntryBody from './SlideEntryBody';
import Action from './Action';
import colors from './common/colors';
import { milesToMin } from './utils';

export interface IRestaurantData {
    title: string,
    image: string,
    phone: string,
    url: string,
    address: string,
    open_closed: string;
    distance: number;
    rating: number;
}

interface Props {
    address: string;
    data: IRestaurantData;
    parallaxProps: any;
    onPress: () => void;
}

const { width, height } = Dimensions.get('window');

const containerWidth = width * .75;
const itemHorizontalMargin = width * .01;

const itemHeight = height * .6;

export const itemWidth = containerWidth + itemHorizontalMargin * 2;
export const sliderWidth = width;

export default class SliderEntry extends PureComponent<Props> {
    callRestaurant = () => {
        if (this.props.data.phone) {
            Linking.openURL(`tel:${this.props.data.phone}`);
        }
    }

    openDirections = () => {
        const origin = encodeURIComponent(this.props.address);
        const destination = encodeURIComponent(this.props.data.address);
        const url = `https://www.google.com/maps/dir/?api=1&origin=${origin}&destination=${destination}`;

        if (Linking.canOpenURL(url)) {
            Linking.openURL(url);
        }
    }

    saveRestaurant = async () => {
        const savedRestaurants = await AsyncStorage.getItem('savedRestaurants');
        const { title } = this.props.data;

        if (savedRestaurants) {
            const restaurants = JSON.parse(savedRestaurants);
            if (!~restaurants.indexOf(title)) {
                restaurants.push(title);
                await AsyncStorage.setItem('savedRestaurants', JSON.stringify(restaurants));
            }
        } else {
            const payload = JSON.stringify([title]);
            await AsyncStorage.setItem('savedRestaurants', payload);
        }
    }

    renderImage(imageUri: string, parallaxProps: any) {
        return <ParallaxImage
            source={{ uri: imageUri }}
            containerStyle={[styles.imageContainer]}
            style={styles.image}
            parallaxFactor={0.35}
            showSpinner={true}
            spinnerColor='rgba(255, 255, 255, 0.4)'
            {...parallaxProps}
        />
    };

    render() {
        const { data, parallaxProps, onPress} = this.props;

        return (
            <View style={styles.shadow} elevation={2}>
            <TouchableOpacity
                activeOpacity={.9}
                style={styles.container}
                onPress={onPress}>
                <SlideEntryHeader title={data.title} onPress={this.saveRestaurant} />
                <View style={styles.details}>
                    <RestaurantDetail type={RestaurantDetailType.DISTANCE} value={milesToMin(data.distance)} />
                    <RestaurantDetail type={RestaurantDetailType.PRICE_RANGE} value='--' />
                    <RestaurantDetail type={RestaurantDetailType.RATING} value={data.rating} />
                    <RestaurantDetail type={RestaurantDetailType.FAVORITES} value='--' />
                </View>
                <View style={styles.imageContainer}>
                    <ParallaxImage
                        source={{ uri: data.image }}
                        containerStyle={[styles.imageContainer]}
                        style={styles.image}
                        parallaxFactor={0.35}
                        showSpinner={true}
                        spinnerColor='rgba(255, 255, 255, 0.4)'
                        {...parallaxProps} />
                    <View style={styles.imageOverlay}>
                        <SlideEntryBody address={data.address} openClosed={data.open_closed} />
                    </View>
                </View>
                <View style={styles.actions}>
                    <Action text="Call" onPress={this.callRestaurant} />
                    <Action text={`${data.distance.toFixed(1)} miles away`} onPress={this.openDirections} />
                    <Action text="Save" onPress={this.saveRestaurant} />
                </View>
            </TouchableOpacity>
            </View>
        );
    }
}

const styles =  StyleSheet.create({
    container: {
        width: itemWidth,
        height: itemHeight,
        overflow: 'hidden',
        borderRadius: 12,
    },
    shadow: {
        shadowOffset: { width: 1, height: 3 },
        shadowColor: 'rgb(40, 40, 40)',
        shadowOpacity: .6,
    },
    imageContainer: {
        flex: 1,
    },
    details: {
        backgroundColor: colors.white,
        flexDirection: 'row',
        paddingVertical: 5
    },
    image: {
        resizeMode: 'cover',
    },
    imageOverlay: {
        ...StyleSheet.absoluteFillObject
    },
    actions: {
        flexDirection: 'row',
        backgroundColor: colors.white,
        justifyContent: 'space-around',
        alignItems: 'center',
        height: 50,
    }
});