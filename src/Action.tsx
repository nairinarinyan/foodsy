import React from 'react';
import {
    TouchableOpacity,
    Text,
    StyleSheet
} from 'react-native';
import colors from './common/colors';

interface Props {
    text: string;
    onPress?: () => void;
}

const Action = (props: Props) => {
    return (
        <TouchableOpacity activeOpacity={.5} onPress={props.onPress}>
            <Text style={styles.text}>{props.text}</Text>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    text: {
        fontWeight: '400',
        fontSize: 16,
        color: colors.blue
    }
});

export default Action;