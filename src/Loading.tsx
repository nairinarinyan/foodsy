import React from 'react';
import {
    View,
    ActivityIndicator,
    StyleSheet,
    Platform
} from 'react-native';

const Loading = () => (
    <View style={styles.container}>
        <ActivityIndicator size={Platform.select({
            android: 50,
            ios: 1
        })} />
    </View>
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default Loading;