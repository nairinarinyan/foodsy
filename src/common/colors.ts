export default {
    blue: '#005ef2',
    lightBlue: '#a8dcf6',
    white: '#FEFEFE',
    red: '#E12D17'
};