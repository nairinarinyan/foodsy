import React from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import colors from './common/colors';

export enum RestaurantDetailType {
    DISTANCE,
    PRICE_RANGE,
    RATING,
    FAVORITES
}

interface Props {
    type: RestaurantDetailType;
    value: string | number;
}

const renderIcon = (type: RestaurantDetailType) => {
    switch(type) {
        case RestaurantDetailType.DISTANCE:
            return [<MaterialIcon name='directions-walk' color={colors.blue} size={20}/>, 'min'];
        case RestaurantDetailType.PRICE_RANGE:
            return [<MaterialIcon name='attach-money' color={colors.blue} size={20}/>, '']
        case RestaurantDetailType.RATING:
            return [<MaterialIcon name='stars' color={colors.blue} size={20}/>, '/5']
        case RestaurantDetailType.FAVORITES:
            return [<MaterialIcon name='favorite' color={colors.blue} size={20}/>, '']
    }
}

const RestaurantDetail = (props: Props) => {
    const [comp, unit] = renderIcon(props.type);

    return (
        <View style={styles.container}>
            {comp}
            <Text>{props.value}</Text>
            <Text style={styles.unit}>{unit}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    unit: {
        fontSize: 10
    }
});

export default RestaurantDetail;