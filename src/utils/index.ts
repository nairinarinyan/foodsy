export const milesToMin = (mile: number): number => {
    const speed = 3;
    const hours = mile / speed;
    return hours * 60 << 0;
};