import React from 'react';
import {
    View,
    StyleSheet,
    Text
} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import colors from './common/colors';

interface Props {
    address: string;
    openClosed: string;
}

const SliderEntryBody = (props: Props) => (
    <View style={styles.container}>
        <View>
            <View style={styles.location}>
                <MaterialIcon name="location-on" color={colors.white} size={20} style={styles.icon} />
                <Text style={styles.locationText}>{props.address}</Text>
            </View>
            <Text style={styles.closing}>{props.openClosed}</Text>
        </View>
        <View style={styles.bottom}>
        </View>
    </View>
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 20,
        paddingHorizontal: 20,
        backgroundColor: 'rgba(0, 0, 0, .4)'
    },
    icon: {
        marginRight: 10
    },
    location: {
        flexDirection: 'row',
        marginBottom: 5
    },
    locationText: {
        flex: 1,
        flexWrap: 'wrap',
        fontWeight: '500',
        color: colors.white,
        fontSize: 15,
    },
    closing: {
        marginLeft: 30,
        fontSize: 13,
        color: colors.white,
    },
    bottom: {

    }
});

export default SliderEntryBody;