import React, { PureComponent } from 'react';
import { Navigation } from 'react-native-navigation';
import {
    View,
    StyleSheet,
    TextInput,
    Text,
    TouchableOpacity,
    Button,
    Alert
} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import colors from '../common/colors';
import { bottomTabConfig } from '.';

const googleMapsApiKey = 'AIzaSyDcO42TVYHXiQqGOWvRAiQgOSWdwoLMrMg';

interface Props {}

interface State {
    address: string;
}

export default class EntryScreen extends PureComponent<Props, State> {
    state = {
        address: '1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA'
    }

    onChange = (address: string) => {
        this.setState({ address });
    }

    onLocationPress = () => {
        navigator.geolocation.getCurrentPosition(async (position: any) => {
            const resolvedAddress = await this.resolveAddress(position);
            this.setState({ address: resolvedAddress });
        }, (err: any) => {
            Alert.alert('Error getting location', err.message);
        });
    }

    resolveAddress(position: any) {
        const { latitude, longitude } = position.coords;

        return fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + latitude + ',' + longitude + '&key=' + googleMapsApiKey)
            .then(res => res.json())
            .then(response => {
                return response.results[0].formatted_address;
            });
    }

    onSubmit = () => {
        Navigation.setRoot({
            root: {
                bottomTabs: bottomTabConfig({
                    address: this.state.address
                }, {})
            }
        });
    }

    render() {
        const { address } = this.state;

        return (
            <View style={styles.container}>
                <View style={styles.messageContainer} elevation={2}>
                    <Text style={styles.title}>Foodsy</Text>
                    <Text style={styles.message}>Tap on the icon to autofill your location</Text>
                    <Text style={styles.message}>Or enter it manually</Text>
                </View>
                <View style={styles.inputContainer} elevation={2}>
                    <TextInput
                        value={address}
                        onChangeText={this.onChange}
                        underlineColorAndroid={colors.blue}
                        placeholder='Your Address'
                        style={styles.textInput}
                    />
                    <TouchableOpacity activeOpacity={.5} onPress={this.onLocationPress}>
                        <MaterialIcon
                            name='my-location'
                            color={colors.blue}
                            size={20}
                            style={styles.icon}
                        />
                    </TouchableOpacity>
                </View>
                    <View style={styles.buttonContainer} elevation={2}>
                        <TouchableOpacity onPress={this.onSubmit} activeOpacity={.5} style={styles.touchable}>
                            <View style={styles.submitButton}>
                                <Text style={styles.submitText}>Submit</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
            </View>
        );
    }
}

const textStyle = StyleSheet.create({
    text: {
        fontWeight: 'bold',
        textAlign: 'center',
        color: colors.blue
    }
});

const cardStyle = StyleSheet.create({
    card: {
        width: '80%',
        borderRadius: 5,
        backgroundColor: colors.white,
    }
});

const shadowStyle = StyleSheet.create({
    shadow: {
        shadowOffset: { width: 0, height: 3 },
        shadowColor: 'rgb(40, 40, 40)',
        shadowOpacity: .4
    }
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.lightBlue
    },
    messageContainer: {
        paddingVertical: 10,
        paddingHorizontal: 20,
        marginBottom: 20,
        ...cardStyle.card as any,
        ...shadowStyle.shadow as any,
    },
    title: {
        ...textStyle.text as any,
        fontSize: 24,
        marginBottom: 10
    },
    message: {
        ...textStyle.text as any,
        fontSize: 14
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        marginBottom: 10,
        ...cardStyle.card as any,
        ...shadowStyle.shadow as any
    },
    textInput: {
        flex: 1,
        paddingHorizontal: 10,
        paddingVertical: 20
    },
    icon: {
        marginLeft: 10
    },
    buttonContainer: {
        ...cardStyle.card as any,
        backgroundColor: colors.blue
    },
    submitButton: {
        padding: 10
    },
    submitText: {
        ...textStyle.text as any,
        textAlign: 'center',
        color: colors.white
    }
});