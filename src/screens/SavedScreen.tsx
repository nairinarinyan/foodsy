import React, { PureComponent } from 'react'
import { Navigation } from 'react-native-navigation';
import {
    View,
    StyleSheet,
    Text,
    ScrollView,
    AsyncStorage,
    TouchableOpacity
} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import colors from '../common/colors';

interface Props {}
interface State {
    savedRestaurants: string[];
}

export default class SavedScreen extends PureComponent<Props, State> {
    constructor(props: any) {
        super(props);

        this.state = {
            savedRestaurants: []
        };

        Navigation.events().bindComponent(this);
    }

    componentDidAppear() {
        this.fetchSavedRestaurants();
    }

    async fetchSavedRestaurants() {
        const savedRestaurants = await AsyncStorage.getItem('savedRestaurants');

        if (savedRestaurants) {
            this.setState({
                savedRestaurants: JSON.parse(savedRestaurants)
            });
        }
    }

    async delete(restaurantName: string) {
        const savedRestaurants = await AsyncStorage.getItem('savedRestaurants');

        if (savedRestaurants) {
            const restaurants = JSON.parse(savedRestaurants);
            const restaurantIdx = restaurants.indexOf(restaurantName);
            restaurants.splice(restaurantIdx, 1);
            await AsyncStorage.setItem('savedRestaurants', JSON.stringify(restaurants));
            this.fetchSavedRestaurants();
        }
    }

    render() {
        const { savedRestaurants } = this.state;

        return (
            <View style={styles.container}>
                <ScrollView contentContainerStyle={styles.scrollContainer}>
                    {savedRestaurants.map((restaurantName: string) =>
                        <View key={restaurantName} style={styles.restaurant} elevation={2}>
                            <Text style={styles.text}>{restaurantName}</Text>
                            <TouchableOpacity onPress={() => this.delete(restaurantName)}>
                                <MaterialIcon name='highlight-off' color={colors.red} size={20} />
                            </TouchableOpacity>
                        </View>
                    )}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.lightBlue
    },
    scrollContainer: {
        padding: 10
    },
    restaurant: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: 14,
        paddingHorizontal: 20,
        marginBottom: 10,
        borderRadius: 5,
        backgroundColor: colors.white,
        shadowOffset: { width: 0, height: 3 },
        shadowColor: 'rgb(40, 40, 40)',
        shadowOpacity: .4
    },
    text: {
        color: colors.blue,
        fontSize: 16
    },
    deleteText: {
        color: colors.red,
        fontSize: 14
    }
});