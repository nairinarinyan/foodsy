import { Navigation } from 'react-native-navigation';
import EntryScreen from './EntryScreen';
import SavedScreen from './SavedScreen';
import colors from '../common/colors';

export const initScreens = (Root: any) => {
    Navigation.registerComponent('foodsy.RootScreen', () => Root);
    Navigation.registerComponent('foodsy.EntryScreen', () => EntryScreen);
    Navigation.registerComponent('foodsy.SavedScreen', () => SavedScreen);
}

export const bottomTabConfig = (firstTabProps: any, secondTabProps: any) => ({
    children: [
        {
            stack: {
                children: [{
                    component: {
                        name: 'foodsy.RootScreen',
                        passProps: firstTabProps,
                    }
                }],
                options: {
                    topBar: {
                        title: {
                            text: 'Foodsy'
                        }
                    },
                    bottomTab: {
                        icon: require('../assets/images/carousel.png'),
                        selectedIconColor: colors.white,
                        iconColor: colors.lightBlue,
                        iconInsets: { top: 6, bottom: -6, left: 0, right: 0 }
                    }
                }
            }
        },
        {
            stack: {
                children: [{
                    component: {
                        name: 'foodsy.SavedScreen',
                        passProps: secondTabProps,
                    }
                }],
                options: {
                    topBar: {
                        title: {
                            text: 'Saved'
                        }
                    },
                    bottomTab: {
                        icon: require('../assets/images/star.png'),
                        selectedIconColor: colors.white,
                        iconColor: colors.lightBlue,
                        iconInsets: { top: 6, bottom: -6, left: 0, right: 0 }
                    }
                }
            }
        }
    ],
    options: {
        bottomTabs: {
            titleDisplayMode: 'alwaysHide',
            backgroundColor: colors.blue
        }
    }
});