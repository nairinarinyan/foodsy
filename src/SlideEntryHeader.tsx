import React from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Text,
} from 'react-native';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from './common/colors';

interface Props {
    onPress: () => void;
    title: string;
}

const SlideEntryHeader = (props: Props) => (
    <View style={styles.container}>
        <Text style={styles.title}>{props.title}</Text>
        <TouchableOpacity onPress={props.onPress}>
            <View style={styles.pinCircle}>
                <MaterialCommunityIcon name='pin' color={colors.blue} size={20}/>
            </View>
        </TouchableOpacity>
    </View>
);

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: colors.blue,
        paddingHorizontal: 20,
        paddingVertical: 10,
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold',
        letterSpacing: 0.5,
        color: colors.white
    },
    pinCircle: {
        width: 30,
        height: 30,
        borderRadius: 15,
        backgroundColor: colors.white,
        alignItems: 'center',
        justifyContent: 'center',
        transform: [{
            rotate: '45deg'
        }]
    },
});

export default SlideEntryHeader;